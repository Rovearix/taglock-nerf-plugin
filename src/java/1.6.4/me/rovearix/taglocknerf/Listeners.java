package me.rovearix.taglocknerf;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Listeners implements Listener {
    public static TaglockNerf plugin;

    public Listeners(TaglockNerf plugin) {
        this.plugin = plugin;
    }

    //Nerfs the amount of taglock kits you can right click with
    @EventHandler
    public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent event) {

        //Get the item the player is holding
        ItemStack item = event.getPlayer().getItemInHand();

        //Checks for unbound tag lock
        if (item != null && item.getType().toString().contains("X7803")) {

            //Checks for player interaction
            if (event.getRightClicked() instanceof Player) {

                //Check for config amount of tag lock kits
                int limit = plugin.getConfig().getInt("limit");

                //Check if the item is a tag lock kit
                if (item.getAmount() > limit) {

                    //Send the player a warning message
                    event.getPlayer().sendRawMessage("Taglocks are limited to " + limit + " at a time");

                    //Store the original amount of tag lock kits
                    int amount = item.getAmount();

                    //Split apart the tag lock kits
                    item.setAmount(limit);
                    ItemStack[] inventory = event.getPlayer().getInventory().getContents();

                    //Check config for where to start
                    for (int slot = plugin.getConfig().getBoolean("hotbar") ? 0 : 9; slot < inventory.length && amount > 0; slot++)
                        if (inventory[slot] == null) {
                            inventory[slot] = item.clone();
                            inventory[slot].setAmount(amount - limit);
                            amount = 0;
                        }

                    //Returns updated inventory to player
                    event.getPlayer().getInventory().setContents(inventory);

                    //Drops extra tag lock kits if inventory full
                    if (amount != 0)
                        event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), new ItemStack(item.getType(), amount - limit));

                }
            }
        }
    }
}