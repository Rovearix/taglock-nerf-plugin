package me.rovearix.taglocknerf;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class TaglockNerf extends JavaPlugin {

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        Bukkit.getPluginManager().registerEvents(new Listeners(this), this);
    }
}
