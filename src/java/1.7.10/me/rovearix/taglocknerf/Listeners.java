package me.rovearix.taglocknerf;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Listeners implements Listener {
    public static TaglockNerf plugin;

    public Listeners(TaglockNerf plugin) {
        this.plugin = plugin;
    }

    //Nerfs the amount of taglock kits you can right click with
    @EventHandler
    public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent event) {

        //Get the item the player is holding
        ItemStack item = event.getPlayer().getItemInHand();

        //Checks for taglock
        if (item != null && item.getType().toString().equals("WITCHERY_TAGLOCKKIT")) {


            //Checks for player interaction
            if (event.getRightClicked() instanceof Player) {

                //TODO: Check for Mob binding
//                if (item.getData().toString().equals("WITCHERY_TAGLOCKKIT(1)") && item.getItemMeta() != null) {
//
//                    //Grabs the metadata's lore from the item
//                    ItemMeta meta = item.getItemMeta();
//                    List<String> lore = meta.getLore();
//
//                    //Reset the Non-player tag
//                    if (lore != null && lore.size() == 1) {
//                        lore.set(0, "Reset");
//                        meta.setLore(lore);
//
//                        //Return metadata back to the item
//                        item.setItemMeta(meta);
//                    }
//                }

                //Check for config amount of tag lock kits
                int limit = plugin.getConfig().getInt("limit");

                //Check if the item is a taglock kit
                if (item.getAmount() > limit) {

                    //Send the player a warning message
                    event.getPlayer().sendRawMessage("Taglocks are limited to " + limit + " at a time");

                    //Store the original amount of taglock kits
                    int amount = item.getAmount();

                    //Split apart the tag lock kits
                    item.setAmount(limit);
                    ItemStack[] inventory = event.getPlayer().getInventory().getContents();

                    //Check config for where to start
                    for (int slot = plugin.getConfig().getBoolean("hotbar") ? 0 : 9; slot < inventory.length && amount > 0; slot++)
                        if (inventory[slot] == null) {
                            inventory[slot] = item.clone();
                            inventory[slot].setAmount(amount - limit);
                            amount = 0;
                        }

                    //Returns updated inventory to player
                    event.getPlayer().getInventory().setContents(inventory);

                    //Drops extra taglock kits if inventory full
                    if (amount != 0)
                        event.getPlayer().getWorld().dropItemNaturally(event.getPlayer().getLocation(), new ItemStack(item.getType(), amount - limit));
                }
            }
            //TODO: Add Handler for mob interaction
//            else {
//
//                //Grab players inventory
//                ItemStack[] inventory = event.getPlayer().getInventory().getContents();
//
//                //Inject encoding into new animal taglock
//                for (int slot = 0; slot < inventory.length; slot++)
//                    if (inventory[slot] != null && inventory[slot].getData().toString().equals("WITCHERY_TAGLOCKKIT(1)") && (inventory[slot].getItemMeta().getLore() == null || inventory[slot].getItemMeta().getLore().size() == 1)) {
//
//                        //Generate non-player metadata
//                        List<String> lore = inventory[slot].getItemMeta().getLore();
//                        lore.add("Non-Player");
//
//                        //Send new metadata to the taglock
//                        ItemMeta meta = inventory[slot].getItemMeta();
//                        meta.setLore(lore);
//                        inventory[slot].setItemMeta(meta);
//                    }
//
//                //Returns updated inventory to player
//                event.getPlayer().getInventory().setContents(inventory);
//
//                //Checks for taglock date
//                ExpireUtils.check(event);
//            }
        }
    }

    //Checks for taglock date
    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        ExpireUtils.check(event);
    }

    //Checks for taglock date
    @EventHandler
    public void onPlayerItemHeldEvent(PlayerItemHeldEvent event) {
        ExpireUtils.check(event);
    }
}