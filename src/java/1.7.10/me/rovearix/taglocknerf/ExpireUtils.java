package me.rovearix.taglocknerf;

import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ExpireUtils {

    //Checks expiration date data
    public static void check(PlayerEvent event) {

        //Checks inventory for bound taglock kit
        ItemStack[] inventory = event.getPlayer().getInventory().getContents();
        for (int slot = 0; slot < inventory.length; slot++) {
            ItemStack item = inventory[slot];
            if (item != null && item.getData().toString().equals("WITCHERY_TAGLOCKKIT(1)")) {

                //Checks if the taglock kit has a date
                ItemMeta meta = item.getItemMeta();
                List<String> lore = meta.getLore();
                if (meta.getLore() == null || lore.get(0).equals("Reset")) {

                    //Gives taglock kit expiration date
                    meta.setLore(getExpirationDate());
                    item.setItemMeta(meta);
                } else if (lore.size() == 3) {

                    //Check taglock for remaining time
                    long timeLeft = Long.parseLong(lore.get(2)) - System.currentTimeMillis();
                    if (timeLeft > 0) {
                        lore.set(1, timeLeft / (24 * 60 * 60 * 1000) + " Days Remaining");
                        meta.setLore(lore);
                        item.setItemMeta(meta);
                    } else {

                        //Revert the expired taglock kits
                        inventory[slot] = new ItemStack(item.getType(), item.getAmount());

                        //Option to mark the expired taglock kits
                        if (Listeners.plugin.getConfig().getBoolean("expiredLore")) {
                            ArrayList<String> usedLore = new ArrayList<>();
                            usedLore.add("Expired Tag Lock");
                            meta.setLore(usedLore);
                            inventory[slot].setItemMeta(meta);
                        }

                        //Returns updated inventory to player
                        event.getPlayer().getInventory().setContents(inventory);
                    }
                }
            }
        }
    }

    //Returns an ArrayList with the correct expiration date
    public static ArrayList<String> getExpirationDate() {

        //Acquires the set days
        int days = Listeners.plugin.getConfig().getInt("days");

        //Generates the message
        ArrayList<String> lore = new ArrayList<>();
        lore.add("Expiration Date");
        lore.add(days + " Days Remaining");
        lore.add("" + (System.currentTimeMillis() + (long) ((days + .001) * 24 * 60 * 60 * 1000)));
        return lore;
    }
}
